// vim:set sw=4 ts=4 sts=4 ft=javascript expandtab:
var svg;
var workCanvas     = 'Work with canvas';
var workSvg        = 'Work with SVG';
var customImgText  = 'Custom image';
var sorryNoGallery = 'Sorry, but you haven\'t saved any image yet';
var load           = 'Load';
var deleteText     = 'Delete';
var download       = 'Download';
var downloadClic   = 'Please, right-click on the sticker and select "Save as" to download the sticker'
var importOk       = 'Your file has been successfully imported';
var customImg;

var fonts = {
    'Amarante': {
        'regular'     : 'fonts/amarante/Amarante.woff2',
        'bold'        : 'fonts/amarante/Amarante.woff2',
        'italic'      : 'fonts/amarante/Amarante.woff2',
        'italic bold' : 'fonts/amarante/Amarante.woff2',
    },
    'Comic Neue': {
        'regular'     : 'fonts/comic-neue/ComicNeue-Regular.woff2',
        'bold'        : 'fonts/comic-neue/ComicNeue-Bold.woff2',
        'italic'      : 'fonts/comic-neue/ComicNeue-Regular-Oblique.woff2',
        'italic bold' : 'fonts/comic-neue/ComicNeue-Bold-Oblique.woff2',
    },
    'Comic Spice': {
        'regular'     : 'fonts/comic-spice/ComicSpice.woff2',
        'bold'        : 'fonts/comic-spice/ComicSpice.woff2',
        'italic'      : 'fonts/comic-spice/ComicSpice.woff2',
        'italic bold' : 'fonts/comic-spice/ComicSpice.woff2',
    },
    'Hack': {
        'regular'     : 'fonts/hack/hack-regular-webfont.woff2',
        'bold'        : 'fonts/hack/hack-bold-webfont.woff2',
        'italic'      : 'fonts/hack/hack-italic-webfont.woff2',
        'italic bold' : 'fonts/hack/hack-bolditalic-webfont.woff2',
    },
    'Open Sans': {
        'regular'     : 'fonts/open-sans/OpenSans-Regular.woff2',
        'bold'        : 'fonts/open-sans/OpenSans-Bold.woff2',
        'italic'      : 'fonts/open-sans/OpenSans-Italic.woff2',
        'italic bold' : 'fonts/open-sans/OpenSans-BoldItalic.woff2'
    },
    'Some Time Later': {
        'regular'     : 'fonts/some-time-later/Some-Time-Later.woff2',
        'bold'        : 'fonts/some-time-later/Some-Time-Later.woff2',
        'italic'      : 'fonts/some-time-later/Some-Time-Later.woff2',
        'italic bold' : 'fonts/some-time-later/Some-Time-Later.woff2',
    },
    'Nosifer': {
        'regular'     : 'fonts/nosifer/nosifer.woff2',
        'bold'        : 'fonts/nosifer/nosifer.woff2',
        'italic'      : 'fonts/nosifer/nosifer.woff2',
        'italic bold' : 'fonts/nosifer/nosifer.woff2',
    },
    'UnifrakturMaguntia': {
        'regular'     : 'fonts/unifraktur-maguntia/UnifrakturMaguntia.woff2',
        'bold'        : 'fonts/unifraktur-maguntia/UnifrakturMaguntia.woff2',
        'italic'      : 'fonts/unifraktur-maguntia/UnifrakturMaguntia.woff2',
        'italic bold' : 'fonts/unifraktur-maguntia/UnifrakturMaguntia.woff2',
    },
    'Xolonium': {
        'regular'     : 'fonts/xolonium/Xolonium-Regular.woff2',
        'bold'        : 'fonts/xolonium/Xolonium-Bold.woff2',
        'italic'      : 'fonts/xolonium/Xolonium-Regular.woff2',
        'italic bold' : 'fonts/xolonium/Xolonium-Bold.woff2',
    }
};

var loadedFonts = {};

$(document).ready(function() {
    detectLang();
    getParameters();
    bindEvents();
    toggleBgSettings();
    refreshFilename();
    // Load configured font before drawing the sticker
    waitForfont('wemake-font', function() {
        waitForfont('porn-font');
    });
});

function detectLang() {
    var languages = [];
    var clang    = readCookie('lang');
    if (clang !== undefined && clang !== null) {
        languages.push(clang);
    }
    var nl        = navigator.languages;
    for (var i = 0; i < nl.length; i++) {
        languages.push(nl[i]);
    }
    if (languages.length !== 0) {
        translate(languages.shift(), languages);
    } else {
        console.log('Using old style language detection.');
        var userLang = navigator.language || navigator.userLanguage;
        translate(userLang);
    }
}

function translate(userLang, languages) {
    // Try to get a translation
    $.get({
        url: 'locales/'+userLang+'.json',
        dataType: 'json',
        error: function() {
            if (languages !== undefined && languages !== null && languages.length !== 0) {
                translate(languages.shift(), languages);
            }
        },
        success: function(locale) {
            toggleI18n(userLang);
            if (userLang === 'groot') {
                $('#wemake').val('I am');
                $('#porn').val('Groot');
                $('#bg-type').val('img');
                $('#bg-img').val('bark');
                $('#wemake-color').val('#ffffff');
                $('#porn-color').val('#ffffff');
                $('#rect-color').val('#ffffff');
                toggleBgSettings();
                refreshFilename();
                refreshText();
            }
            ['get-source', 'license', 'author', 'wemake', 'porn', 'border-txt',
                'color', 'transparency', 'x-position', 'y-position', 'font', 'style', 'text-size',
                'text-width', 'outline-color', 'outline-size'
            ].forEach(function(e) {
                if (locale[e] !== undefined) {
                    $('.'+e).text(locale[e]);
                }
            });
            ['hide-settings', 'set-corner-shape', 'set-colors', 'set-fonts', 'set-bg', 'set-positions',
                'set-img-size', 'gallery'
            ].forEach(function(e) {
                if (locale[e] !== undefined) {
                    $('[data-setting="'+e+'"]').text(locale[e]);
                }
            });
            ['wemake', 'porn', 'filename', 'corners', 'bg-type', 'bgu-color', 'bgr-color', 'egr-color',
                'gr-orientation', 'bg-img', 'custom-img', 'custom-x', 'custom-y', 'custom-size', 'border-thickness',
                'img-width', 'img-height', 'custom-img-url', 'rick-size', 'rick-x', 'rick-y', 'rick-rad'
            ].forEach(function(e) {
                if (locale[e] !== undefined) {
                    $('label[for="'+e+'"]').text(locale[e]);
                }
            });
            if (locale['corners-rounded'] !== undefined) {
                $('#corners option[value="20"]').text(locale['corners-rounded']);
            }
            if (locale['corners-square'] !== undefined) {
                $('#corners option[value="0"]').text(locale['corners-square']);
            }
            if (locale['add-glitter'] !== undefined) {
                $('.glitter span').text(locale['add-glitter']);
            }
            ['plain', 'gradient', 'rainbow', 'img', 'lr', 'rl', 'tb', 'bt', 'r', 'beer', 'glitter',
                'rainbow-flag', 'sky', 'field', 'bridge', 'regular', 'bold', 'italic', 'italic bold',
                'bark', 'snow', 'whale'
            ].forEach(function(e) {
                if (locale[e] !== undefined) {
                    $('option[value="'+e+'"]').text(locale[e]);
                }
            });
            ['settings-dpdn', 'toggle-svg', 'download', 'reset', 'switch-colors',
                'make-text-center', 'print', 'save', 'import', 'export'
            ].forEach(function(e) {
                if (locale[e] !== undefined) {
                    if (e === 'download') {
                        download = locale[e]
                    }
                    $('#'+e).text(locale[e]);
                }
            });
            if (locale['i18n-dpdn'] !== undefined) {
                $('#i18n-dpdn i').attr('title', locale['i18n-dpdn']);
            }
            if (locale['import-n-export'] !== undefined) {
                $('#import-n-export').attr('title', locale['import-n-export']);
            }
            if (locale['toggle-canvas'] !== undefined) {
                workCanvas     = locale['toggle-canvas'];
            }
            if (locale['toggle-svg'] !== undefined) {
                workSvg        = locale['toggle-svg'];
            }
            if (locale['sorry-no-gallery'] !== undefined) {
                sorryNoGallery = locale['sorry-no-gallery'];
            }
            if (locale['load'] !== undefined) {
                load           = locale['load'];
            }
            if (locale['delete'] !== undefined) {
                deleteText     = locale['delete'];
            }
            if (locale['custom-img-text'] !== undefined) {
                customImgText  = locale['custom-img-text'];
                var c = $('option[value="custom"]');
                if (c.length !== 0) {
                    c.text(customImgText);
                }
            }
            if (locale['import-ok'] !== undefined) {
                importOk       = locale['import-ok'];
            }
            if (locale['download-clic'] !== undefined) {
                downloadClic   = locale['download-clic'];
            }
            if (locale['add-rick'] !== undefined) {
                $('.rick span').text(locale['add-rick']);
            }
        }
    });
}

function getParameters() {
    // Get parameters
    if (window.location.hash) {
        $('#porn').val(decodeURIComponent(window.location.hash.substr(1)));
    }
    var url = new URL(window.location);
    if (url.searchParams.get('w') !== null) {
        $('#wemake').val(url.searchParams.get('w'));
    }
    if (url.searchParams.get('wx') !== null) {
        $('#wemake-x').val(url.searchParams.get('wx'));
    }
    if (url.searchParams.get('wy') !== null) {
        $('#wemake-y').val(url.searchParams.get('wy'));
    }
    if (url.searchParams.get('ws') !== null) {
        $('#wemake-size').val(url.searchParams.get('ws'));
    }
    if (url.searchParams.get('wc') !== null) {
        $('#wemake-color').val(url.searchParams.get('wc'));
    }
    if (url.searchParams.get('wa') !== null) {
        $('#wemake-alpha').val(url.searchParams.get('wa'));
    }
    if (url.searchParams.get('wf') !== null) {
        $('#wemake-font').val(url.searchParams.get('wf'));
    }
    if (url.searchParams.get('wfs') !== null) {
        $('#wemake-font-style').val(url.searchParams.get('wfs'));
    }
    if (url.searchParams.get('ww') !== null) {
        $('#wemake-width').val(url.searchParams.get('ww'));
    }
    if (url.searchParams.get('p') !== null) {
        $('#porn').val(url.searchParams.get('p'));
    }
    if (url.searchParams.get('px') !== null) {
        $('#porn-x').val(url.searchParams.get('px'));
    }
    if (url.searchParams.get('py') !== null) {
        $('#porn-y').val(url.searchParams.get('py'));
    }
    if (url.searchParams.get('ps') !== null) {
        $('#porn-size').val(url.searchParams.get('ps'));
    }
    if (url.searchParams.get('pc') !== null) {
        $('#porn-color').val(url.searchParams.get('pc'));
    }
    if (url.searchParams.get('pa') !== null) {
        $('#porn-alpha').val(url.searchParams.get('pa'));
    }
    if (url.searchParams.get('pf') !== null) {
        $('#porn-font').val(url.searchParams.get('pf'));
    }
    if (url.searchParams.get('pfs') !== null) {
        $('#porn-font-style').val(url.searchParams.get('pfs'));
    }
    if (url.searchParams.get('pw') !== null) {
        $('#porn-width').val(url.searchParams.get('pw'));
    }
    if (url.searchParams.get('co') !== null) {
        $('#bgu-color').val(url.searchParams.get('co'));
    }
    if (url.searchParams.get('gli') !== null) {
        $('#add-glitter').prop('checked', (url.searchParams.get('gli') === 'true') ? true : false);
    }
    if (url.searchParams.get('bgt') !== null) {
        $('#bg-type').val(url.searchParams.get('bgt'));
    }
    if (url.searchParams.get('rc') !== null) {
        $('#rect-color').val(url.searchParams.get('rc'));
    }
    if (url.searchParams.get('ra') !== null) {
        $('#rect-alpha').val(url.searchParams.get('ra'));
    }
    if (url.searchParams.get('bgr') !== null) {
        $('#bgr-color').val(url.searchParams.get('bgr'));
    }
    if (url.searchParams.get('egr') !== null) {
        $('#egr-color').val(url.searchParams.get('egr'));
    }
    if (url.searchParams.get('or') !== null) {
        $('#gr-orientation').val(url.searchParams.get('or'));
    }
    if (url.searchParams.get('bgi') !== null) {
        if (url.searchParams.get('bgi') !== 'custom') {
            $('#bg-img').val(url.searchParams.get('bgi'));
        } else {
            $('#bg-img').val('beer');
        }
    } else {
        $('#bg-img').val('beer');
    }
    if (url.searchParams.get('cor') !== null) {
        $('#corners').val(url.searchParams.get('cor'));
    }
    if (url.searchParams.get('f') !== null) {
        $('#filename').val(url.searchParams.get('f'));
    }
    if (url.searchParams.get('width') !== null) {
        $('#img-width').val(url.searchParams.get('width'));
    }
    if (url.searchParams.get('height') !== null) {
        $('#img-height').val(url.searchParams.get('height'));
    }
    if (url.searchParams.get('bth') !== null) {
        $('#border-thickness').val(url.searchParams.get('bth'));
    }
    if (url.searchParams.get('x') !== null) {
        $('#custom-x').val(url.searchParams.get('x'));
    }
    if (url.searchParams.get('y') !== null) {
        $('#custom-y').val(url.searchParams.get('y'));
    }
    if (url.searchParams.get('s') !== null) {
        $('#custom-size').val(url.searchParams.get('s'));
    }
    if (url.searchParams.get('bgu') !== null && url.searchParams.get('bgu') !== '') {
        $('#custom-img-url').val(url.searchParams.get('bgu'));
        handleImageURL(false);
    }
    if (url.searchParams.get('r') !== null) {
        $('#add-rick').prop('checked', (url.searchParams.get('r') === 'true') ? true : false);
    }
    if (url.searchParams.get('rx') !== null) {
        $('#rick-x').val(url.searchParams.get('rx'));
    }
    if (url.searchParams.get('ry') !== null) {
        $('#rick-y').val(url.searchParams.get('ry'));
    }
    if (url.searchParams.get('rs') !== null) {
        $('#rick-size').val(url.searchParams.get('rs'));
    }
    if (url.searchParams.get('rr') !== null) {
        $('#rick-rad').val(url.searchParams.get('rr'));
    }
    if (url.searchParams.get('woc') !== null) {
        $('#wemake-outline-color').val(url.searchParams.get('woc'));
    }
    if (url.searchParams.get('wos') !== null) {
        $('#wemake-outline-size').val(url.searchParams.get('wos'));
    }
    if (url.searchParams.get('poc') !== null) {
        $('#porn-outline-color').val(url.searchParams.get('poc'));
    }
    if (url.searchParams.get('pos') !== null) {
        $('#porn-outline-size').val(url.searchParams.get('pos'));
    }
}

function bindEvents() {
    // Change text
    $('input[type="text"]').each(function(index) {
        if (this.id !== 'filename') {
            $(this).keyup(function() {
                refreshText();
                refreshFilename();
            });
            $(this).on('paste', function() {
                refreshText();
                refreshFilename();
            });
        }
    });
    $('#custom-img-url').keyup(function() {
        if (this.checkValidity() && !$(this).val().match(/^ *$/)) {
            handleImageURL(true);
            refreshText();
        }
    });
    $('#custom-img-url').on('paste', function() {
        if (this.checkValidity() && !$(this).val().match(/^ *$/)) {
            handleImageURL(true);
            refreshText();
        }
    });

    // Various settings changes
    $('select,input[type="number"],input[type="color"],input[type="checkbox"]').each(function(index) {
        if (this.id !== 'wemake-font' && this.id !== 'wemake-font-style' && this.id !== 'porn-font' && this.id !== 'porn-font-style') {
            if (this.id === 'bg-type' ) {
                $(this).change(function() {
                    if ($(this).val() === 'img' && ($('#bg-img').val() === 'telechat') || $('#bg-img').val() === 'peanuts-nouka') {
                        $('#rect-alpha').val(100);
                        if ($('#bg-img').val() === 'peanuts-nouka') {
                            $('#wemake-x').val('400');
                            $('#wemake-y').val('100');
                            $('#wemake-size').val('90');
                            $('#wemake-color').val('#FFFFFF');
                            $('#wemake-outline-color').val('#000000');
                            $('#wemake-outline-size').val(5);

                            $('#porn-x').val('400');
                            $('#porn-y').val('385');
                            $('#porn-size').val('90');
                            $('#porn-color').val('#FFFFFF');
                            $('#porn-outline-color').val('#000000');
                            $('#porn-outline-size').val(5);
                        }
                    }
                    if ($(this).val() === 'img' && $('#bg-img').val() !== 'custom') {
                        $('#custom-img-url').val('');
                    }
                    refreshText();
                });
            } else if (this.id === 'bg-img') {
                $(this).change(function() {
                    if ($(this).val() === 'telechat' || $('#bg-img').val() === 'peanuts-nouka') {
                        $('#rect-alpha').val(100);
                        if ($('#bg-img').val() === 'peanuts-nouka') {
                            $('#wemake-x').val('400');
                            $('#wemake-y').val('100');
                            $('#wemake-size').val('90');
                            $('#wemake-color').val('#FFFFFF');
                            $('#wemake-outline-color').val('#000000');
                            $('#wemake-outline-size').val(5);

                            $('#porn-x').val('400');
                            $('#porn-y').val('385');
                            $('#porn-size').val('90');
                            $('#porn-color').val('#FFFFFF');
                            $('#porn-outline-color').val('#000000');
                            $('#porn-outline-size').val(5);
                        }
                    }
                    if ($(this).val() !== 'custom') {
                        $('#custom-x').val(0);
                        $('#custom-y').val(0);
                        $('#custom-size').val(1);
                        $('#custom-img-url').val('');
                    }
                    refreshText();
                });
            } else {
                $(this).change(refreshText);
            }
        } else {
            $(this).change(function() {
                waitForfont(this.id);
            });
        }
    });

    // Background type changes
    $('#bg-type').change(toggleBgSettings);

    // Toggle SVG/Canvas
    $('#toggle-svg').click(function(e) {
        e.preventDefault();
        var s = $('#svg');
        if (s.hasClass('d-none')) {
            $(this).removeClass('btn-info');
            $(this).addClass('btn-primary');
            hide('canvas');
            show('svg');
            this.text = workCanvas;
        } else {
            $(this).removeClass('btn-primary');
            $(this).addClass('btn-info');
            hide('svg');
            show('canvas');
            this.text = workSvg;
        }
        refreshText();
        refreshFilename();
    });

    // import data
    $('#import').click(function(e) {
        e.preventDefault();
        $('#import-file').click();
    });

    // Export data
    $('#export').click(exportData);

    // Download
    $('#download').click(function(e) {
        this.download = $('#filename').val();
        var s         = $('#svg');
        if (s.hasClass('d-none')) {
            var bgu = $('#custom-img-url').val();
            if (bgu === undefined || bgu === null || bgu.match(/^ *$/) || !$('#custom-img-url')[0].checkValidity()) {
                this.href     = document.getElementById('canvas').toDataURL('image/octet-stream');
            } else {
                e.preventDefault();
                alert(downloadClic);
            }
        } else {
            this.href     = 'data:image/octet-stream;base64,'+btoa(svg.getSerializedSvg(true));
        }
    });

    // Show or hide settings
    $('[data-setting]').click(function(e) {
        e.preventDefault();
        var t = $(this);
        $('[data-setting]').removeClass('active');
        t.addClass('active');
        if (t.data('setting') === 'hide-settings') {
            hide('gallery');
            show('main-tool');
            ['settings', 'set-corner-shape', 'set-colors', 'set-fonts', 'set-bg', 'set-positions', 'set-img-size', 'set-rick'].forEach(function(e) {
                hide(e);
            });
        } else if (t.data('setting') === 'gallery') {
            hide('main-tool');
            show('gallery');
            fillGallery();
        } else {
            hide('gallery');
            show('main-tool');
            ['set-corner-shape', 'set-colors', 'set-fonts', 'set-bg', 'set-positions', 'set-img-size', 'set-rick'].forEach(function(e) {
                if (e !== t.data('setting')) {
                    hide(e);
                } else {
                    show(e);
                }
                show('settings');
            });
        }
    });

    // Reset settings
    $('#reset').click(function(e) {
        e.preventDefault();
        reset();
    });

    // Switch text colors
    $('#switch-colors').click(function(e) {
        e.preventDefault();
        switchTextColors();
        refreshText();
    });

    // Center text
    $('#make-text-center').click(centerText);

    // Upload custom image
    $('#custom-img').change(handleImage);
    if (customImg === undefined || customImg === null) {
        $('#custom-img').val(null);
    }

    // Choose language
    $('[data-i18n]').click(function(e) {
        e.preventDefault();
        createCookie('lang', $(this).data('i18n'), 30);
        translate($(this).data('i18n'));
    });

    // Print
    $('#print').click(function(e) {
        e.preventDefault();
        window.print();
    });

    // Save
    $('#save').click(function(e) {
        e.preventDefault();
        addItem();
    });
}

function handleImageURL(onChange) {
    $('#bg-type').val('img');
    var o   = $('<option>');
    var b   = $('#bg-img');
    var c   = $('option[value="custom"]');

    if (c.length !== 0) {
        c.remove();
    }

    o.attr('value', 'custom');
    o.text(customImgText);
    b.append(o);
    b.val('custom');

    if (onChange) {
        $('#custom-x').val(0);
        $('#custom-y').val(0);
        $('#custom-size').val(0);
    }

    toggleBgSettings();
}

function toggleBgSettings() {
    switch($('#bg-type').val()) {
        case 'plain':
            show('bg-color');
            hide('gradient-settings');
            hide('bg-img-settings');
            hide('bg-custom-img-settings');
            break;
        case 'gradient':
            hide('bg-color');
            show('gradient-settings');
            hide('bg-img-settings');
            hide('bg-custom-img-settings');
            break;
        case 'img':
            hide('bg-color');
            hide('gradient-settings');
            show('bg-img-settings');
            show('bg-custom-img-settings');
            break;
        default:
            hide('bg-color');
            hide('gradient-settings');
            hide('bg-img-settings');
            hide('bg-custom-img-settings');
    }
}

function refreshFilename() {
    var w = $('#wemake');
    var p = $('#porn');
    var f = $('#filename');
    var s = $('#svg');
    var ext = 'png';

    if (!s.hasClass('d-none')) {
        ext = 'svg';
    }

    var name = w.val()+' '+p.val()+'.'+ext;
    f.val(name.replace(/ /g, '_'));
}

function waitForfont(id, cb) {
    if (cb === undefined || cb === null) {
        cb = refreshText;
    }
    id = id.replace('-style', '');
    var f = $('#'+id).val();
    var s = $('#'+id+'-style').val();
    if (f === 'sans-serif' || (loadedFonts[f] !== undefined && loadedFonts[f][s] !== undefined) || fonts[f] === undefined || fonts[f][s] === undefined) {
        if (f !== 'sans-serif') {
            if (fonts[f] === undefined) {
                console.log(f+' is not a valid font');
            }
            if (fonts[f][s] === undefined) {
                console.log(s+' is not a valid style for font '+f);
            }
        }
        cb();
    } else {
        var myFont = new FontFace(f, 'url('+fonts[f][s]+')');
        myFont.load().then(function(font) {
            console.log('font '+f+' '+s+' loaded');
            document.fonts.add(font);
            if (loadedFonts[f] !== undefined) {
                loadedFonts[f][s] = true;
            } else {
                loadedFonts[f] = {s: true};
            }
            cb();
        });
    }
}

function toggleI18n(lang) {
    $('[data-i18n]').removeClass('active');
    $('[data-i18n="'+lang+'"]').addClass('active');
}

function handleImage(e) {
    var reader    = new FileReader();
    reader.onload = function(event) {
        var img    = new Image();
        img.onload = function() {
            var o   = $('<option>');
            var b   = $('#bg-img');
            var c   = $('option[value="custom"]');

            if (c.length !== 0) {
                c.remove();
            }

            o.attr('value', 'custom');
            o.text(customImgText);
            b.append(o);
            b.val('custom');
            customImg = img;

            $('#custom-x').val(0);
            $('#custom-y').val(0);
            $('#custom-size').val(0);

            refreshText();
        }
        img.src = event.target.result;
    }
    reader.readAsDataURL(e.target.files[0]);
}

function switchTextColors() {
    var w  = $('#wemake-color');
    var p  = $('#porn-color');

    var c1 = w.val();
    var c2 = p.val();

    w.val(c2);
    p.val(c1);
}

function reset() {
    $('#wemake-x').val('400');
    $('#wemake-y').val('160');
    $('#wemake-size').val('150');
    $('#wemake-color').val('#000000');
    $('#wemake-alpha').val('0');
    $('#wemake-font').val('Open Sans');
    $('#wemake-font-style').val('bold');
    $('#wemake-width').val('725');
    $('#porn-x').val('400');
    $('#porn-y').val('350');
    $('#porn-size').val('220');
    $('#porn-color').val('#000000');
    $('#porn-alpha').val('0');
    $('#porn-font').val('Open Sans');
    $('#porn-font-style').val('bold');
    $('#porn-width').val('725');
    $('#bgu-color').val('#fcd205');
    $('#rect-color').val('#000000');
    $('#rect-alpha').val('0');
    $('#add-glitter').prop('checked', false);
    $('#bg-type').val('plain');
    $('#gr-orientation').val('lr');
    $('#bg-img').val('beer');
    $('#corners').val('20');
    $('#custom-img').val(null);
    $('#custom-x').val(0);
    $('#custom-y').val(0);
    $('#custom-size').val(0);
    $('#img-width').val(800);
    $('#img-height').val(400);
    $('#border-thickness').val(20);
    $('#custom-img-url').val(null);
    $('#add-rick').prop('checked', false);
    $('#rick-x').val(0);
    $('#rick-y').val(0);
    $('#rick-size').val(100);
    $('#rick-rad').val(0);
    $('#wemake-outline-color').val('#FFFFFF');
    $('#wemake-outline-size').val(0);
    $('#porn-outline-color').val('#FFFFFF');
    $('#porn-outline-size').val(0);

    show('bg-color');
    hide('gradient-settings');
    hide('bg-img-settings');
    hide('bg-custom-img-settings');

    refreshText();
}

function refreshText() {
    var w   = $('#wemake').val();
    var wx  = $('#wemake-x').val();
    var wy  = $('#wemake-y').val();
    var ws  = $('#wemake-size').val();
    var wc  = $('#wemake-color').val();
    var wa  = $('#wemake-alpha').val();
    var wf  = $('#wemake-font').val();
    var wfs = $('#wemake-font-style').val();
    var ww  = $('#wemake-width').val();
    var p   = $('#porn').val();
    var px  = $('#porn-x').val();
    var py  = $('#porn-y').val();
    var ps  = $('#porn-size').val();
    var pc  = $('#porn-color').val();
    var pa  = $('#porn-alpha').val();
    var pf  = $('#porn-font').val();
    var pfs = $('#porn-font-style').val();
    var pw  = $('#porn-width').val();
    var co  = $('#bgu-color').val();
    var gli = $('#add-glitter').prop('checked');
    var bgt = $('#bg-type').val();
    var rc  = $('#rect-color').val();
    var ra  = $('#rect-alpha').val();
    var bgr = $('#bgr-color').val();
    var egr = $('#egr-color').val();
    var or  = $('#gr-orientation').val();
    var bgi = $('#bg-img').val();
    var cor = $('#corners').val();
    var bth = $('#border-thickness').val();
    var x   = $('#custom-x').val();
    var y   = $('#custom-y').val();
    var s   = $('#custom-size').val();
    var bgu = ($('#custom-img-url')[0].checkValidity()) ? $('#custom-img-url').val() : '';
    var r   = $('#add-rick').prop('checked');
    var rx  = $('#rick-x').val();
    var ry  = $('#rick-y').val();
    var rs  = $('#rick-size').val();
    var rr  = $('#rick-rad').val();
    var woc = $('#wemake-outline-color').val();
    var wos = $('#wemake-outline-size').val();
    var poc = $('#porn-outline-color').val();
    var pos = $('#porn-outline-size').val();

    var width  = $('#img-width').val();
    var height = $('#img-height').val();

    $('#canvas').remove();
    var c    = document.createElement('canvas');
    c.id     = 'canvas';
    c.classList.add('img-fluid');
    c.width  = width;
    c.height = height;
    document.getElementById('canvas-container').appendChild(c);
    var ctx  = c.getContext('2d');

    // do we work on the SVG?
    var svgI = $('#svg');
        svg  = new C2S(width,height);
    if (svgI.hasClass('d-none') === false) {
        ctx = svg;
    }

    // update URL
    var url = new URL(window.location);
    window.history.pushState("change", "WemaWema", createUrl(url,
        w, wx, wy, ws, wc, wa, wf, wfs, ww,
        p, px, py, ps, pc, pa, pf, pfs, pw,
        co, ra, gli, bgt, rc, bgr, egr, or,
        bgi, cor, bth, width, height, x, y,
        s, bgu, r, rx, ry, rs, rr, woc, wos,
        poc, pos)
    );

    drawCtx(false, ctx, rc, ra, ws, wc, wa, wf, wfs, w, wx, wy, ps, pc, pa, pf, pfs, p, px, py, gli, width, height, bth, bgt, co, cor, bgi, s, bgr, egr, or, x, y, bgu, r, rx, ry, rs, rr, ww, pw, woc, wos, poc, pos);
}

function drawCtx(inGallery, ctx, rc, ra, ws, wc, wa, wf, wfs, w, wx, wy, ps, pc, pa, pf, pfs, p, px, py, gli, width, height, bth, bgt, co, cor, bgi, s, bgr, egr, or, x, y, bgu, r, rx, ry, rs, rr, ww, pw, woc, wos, poc, pos) {
    // cleanup
    ctx.clearRect(0, 0, width, height);

    // background color
    switch(bgt) {
        case 'plain':
            ctx.fillStyle = co;
            break;
        case 'gradient':
            var grd;
            switch(or) {
                case 'lr':
                    grd    = ctx.createLinearGradient(0, height/2, width, height/2);
                    break;
                case 'rl':
                    grd    = ctx.createLinearGradient(width, height/2, 0, height/2);
                    break;
                case 'tb':
                    grd    = ctx.createLinearGradient(width/2, 0, width/2, height);
                    break;
                case 'bt':
                    grd    = ctx.createLinearGradient(width/2, height, width/2, 0);
                    break;
                case 'r':
                    grd    = ctx.createRadialGradient(width/2, height/2, 25, width/2, height/2, width/2);
                    break;
            }
            grd.addColorStop(0, bgr);
            grd.addColorStop(1, egr);
            ctx.fillStyle = grd;
            break;
        case 'img':
            if (inGallery !== true && bgi === 'custom' && customImg !== undefined && customImg !== null) {
                var iw    = customImg.width;
                var ih    = customImg.height;
                var sizer = scalePreserveAspectRatio(iw, ih, width, height);
                if (parseFloat(s) === 0.0) {
                    s = Math.round(sizer * 1000) / 1000;
                    if (inGallery !== true) {
                        $('#custom-size').val(s);
                    }
                } else {
                    sizer = s;
                }
                if (inGallery !== true) {
                    var url = new URL(window.location);
                    window.history.pushState("change", "WemaWema", createUrl(url,
                        w, wx, wy, ws, wc, wa, wf, wfs, ww,
                        p, px, py, ps, pc, pa, pf, pfs, pw,
                        co, ra, gli, bgt, rc, bgr, egr, or,
                        bgi, cor, bth, width, height, x, y,
                        s, bgu, r, rx, ry, rs, rr, woc, wos,
                        poc, pos)
                    );
                }
                ctx.drawImage(customImg, 0, 0, iw, ih, x, y, iw*sizer, ih*sizer);
                drawOver(ctx, rc, ra, ws, wc, wa, wf, wfs, w, wx, wy, ps, pc, pa, pf, pfs, p, px, py, gli, width, height, bth, r, rx, ry, rs, rr, ww, pw, woc, wos, poc, pos);
            } else {
                if (bgi === '' || (bgi === 'custom' && (bgu === undefined || bgu === null || bgu === ''))) {
                    bgi = 'beer';
                    if (inGallery !== true && (bgu === undefined || bgu === null || bgu === '')) {
                        $('#bg-img').val('beer');
                    }
                }
                var background = new Image();
                var img        = 'img/'+bgi;
                if (parseInt(cor) === 20) {
                    img += '-round';
                }
                if (bgu === undefined || bgu === null || bgu === '') {
                    img += '.png';
                } else {
                    img = bgu;
                }
                background.src = img;
                background.onload = function() {
                    var iw    = background.width;
                    var ih    = background.height;
                    var sizer = scalePreserveAspectRatio(iw, ih, width, height);
                    if (parseFloat(s) === 0.0) {
                        s = Math.round(sizer * 1000) / 1000;
                        if (inGallery !== true) {
                            $('#custom-size').val(s);
                        }
                    } else {
                        sizer = s;
                    }
                    if (inGallery !== true) {
                        var url = new URL(window.location);
                        window.history.pushState("change", "WemaWema", createUrl(url,
                            w, wx, wy, ws, wc, wa, wf, wfs, ww,
                            p, px, py, ps, pc, pa, pf, pfs, pw,
                            co, ra, gli, bgt, rc, bgr, egr, or,
                            bgi, cor, bth, width, height, x, y,
                            s, bgu, r, rx, ry, rs, rr, woc, wos,
                            poc, pos)
                        );
                    }
                    ctx.drawImage(background, 0, 0, iw, ih, x, y, iw*sizer, ih*sizer);
                    drawOver(ctx, rc, ra, ws, wc, wa, wf, wfs, w, wx, wy, ps, pc, pa, pf, pfs, p, px, py, gli, width, height, bth, r, rx, ry, rs, rr, ww, pw, woc, wos, poc, pos);
                }
            }
            return;
            break;
        case 'rainbow':
            var grd  = ctx.createLinearGradient(width/2, 0, width/2, height);
            var grd2 = svg.createLinearGradient(width/2, 0, width/2, height);
            grd.addColorStop(0, '#e50000');
            grd.addColorStop(1 / 5, '#ff8d00');
            grd.addColorStop(2 / 5, '#ffee00');
            grd.addColorStop(3 / 5, '#008121');
            grd.addColorStop(4 / 5, '#004cff');
            grd.addColorStop(1, '#760188');
            grd2.addColorStop(0, '#e50000');
            grd2.addColorStop(1 / 5, '#ff8d00');
            grd2.addColorStop(2 / 5, '#ffee00');
            grd2.addColorStop(3 / 5, '#008121');
            grd2.addColorStop(4 / 5, '#004cff');
            grd2.addColorStop(1, '#760188');
            ctx.fillStyle = grd;
            svg.fillStyle = grd2;
            break;
    }
    roundRect(ctx, 0, 0, width, height, parseInt(cor), true, false);

    drawOver(ctx, rc, ra, ws, wc, wa, wf, wfs, w, wx, wy, ps, pc, pa, pf, pfs, p, px, py, gli, width, height, bth, r, rx, ry, rs, rr, ww, pw, woc, wos, poc, pos);
}

/*
 * Drawing functions
 */
function drawOver(ctx, rc, ra, ws, wc, wa, wf, wfs, w, wx, wy, ps, pc, pa, pf, pfs, p, px, py, gli, width, height, bth, r, rx, ry, rs, rr, ww, pw, woc, wos, poc, pos) {
    // Inside rounded rectangle
    if (bth > 0) {
        ctx.lineWidth   = bth;
        ctx.strokeStyle = colorToRGBA(rc, ra);
        roundRect(ctx, (width * 1/32), (height * 1/16), (width * 15 / 16), (height * 7 / 8), 20, false);
    }

    ctx.textAlign="center";
    // Write WE MAKE
    var style       = (wfs === 'regular') ? '' : wfs+' ';
    ctx.font        = style+parseInt(ws)+'px \''+wf+'\'';
    ctx.fillStyle   = colorToRGBA(wc, wa);
    if (wos > 0) {
        ctx.strokeStyle = woc;
        ctx.lineWidth   = wos;
        ctx.strokeText(w, parseInt(wx), parseInt(wy), ww);
    }
    ctx.fillText(w, parseInt(wx), parseInt(wy), ww);

    // Write PORN
    style = (pfs === 'regular') ? '' : pfs+' ';
    ctx.font      = style+parseInt(ps)+'px \''+pf;+'\'';
    ctx.fillStyle = colorToRGBA(pc, pa);
    if (pos > 0) {
        ctx.strokeStyle = poc;
        ctx.lineWidth   = pos;
        ctx.strokeText(p, parseInt(px), parseInt(py), pw);
    }
    ctx.fillText(p, parseInt(px), parseInt(py), pw);

    if (r) {
        var rick = new Image();
        rick.src = 'img/Rick.png';
        rick.onload = function() {
            rx = parseInt(rx);
            ry = parseInt(ry);
            var rw = rick.width  * rs / 100;
            var rh = rick.height * rs / 100;
            console.log(rw);
            console.log(rh);
            ctx.translate(rx + (rw / 2), ry + rh / 2);
            ctx.rotate(rr * Math.PI / 180);
            ctx.drawImage(rick, -(rw / 2), -(rh / 2), rw, rh);
            ctx.rotate(-rr * Math.PI / 180);
            ctx.translate(-(rx + rw / 2), -(ry + rh / 2));
            if (gli) {
                var glitter = new Image();
                glitter.src = 'img/glitter-calc.png';
                glitter.onload = function() {
                    ctx.drawImage(glitter, 0, 0);
                    $('#svg').html(svg.getSerializedSvg());
                }
            } else {
                $('#svg').html(svg.getSerializedSvg());
            }
        }
    } else if (gli) {
        var glitter = new Image();
        glitter.src = 'img/glitter-calc.png';
        glitter.onload = function() {
            ctx.drawImage(glitter, 0, 0);
            $('#svg').html(svg.getSerializedSvg());
        }
    } else {
        $('#svg').html(svg.getSerializedSvg());
    }
}

function roundRect(ctx, x, y, width, height, radius, fill, stroke) {
    if (typeof stroke == 'undefined') {
        stroke = true;
    }
    if (typeof radius === 'undefined') {
        radius = 5;
    }
    if (typeof radius === 'number') {
        radius = {tl: radius, tr: radius, br: radius, bl: radius};
    } else {
        var defaultRadius = {tl: 0, tr: 0, br: 0, bl: 0};
        for (var side in defaultRadius) {
            radius[side] = radius[side] || defaultRadius[side];
        }
    }
    ctx.beginPath();
    ctx.moveTo(x + radius.tl, y);
    ctx.lineTo(x + width - radius.tr, y);
    ctx.quadraticCurveTo(x + width, y, x + width, y + radius.tr);
    ctx.lineTo(x + width, y + height - radius.br);
    ctx.quadraticCurveTo(x + width, y + height, x + width - radius.br, y + height);
    ctx.lineTo(x + radius.bl, y + height);
    ctx.quadraticCurveTo(x, y + height, x, y + height - radius.bl);
    ctx.lineTo(x, y + radius.tl);
    ctx.quadraticCurveTo(x, y, x + radius.tl, y);
    ctx.closePath();
    if (fill) {
        ctx.fill();
    }
    if (stroke) {
        ctx.stroke();
    }
}

/*
 * Gallery stuff
 */

function addItem() {
    var wemas = localStorage.getItem('wemas');
    if (wemas === null) {
        wemas = {};
    } else {
        wemas = JSON.parse(wemas);
    }
    var name = $('#filename').val().replace(/\.(png|svg)$/, '');
    var i = 1;
    var tname = name;
    while (wemas[tname] !== undefined && wemas[tname] !== null) {
        tname = name+'-'+i;
        i++;
    }
    wemas[tname] = {
        w      : $('#wemake').val(),
        wx     : $('#wemake-x').val(),
        wy     : $('#wemake-y').val(),
        ws     : $('#wemake-size').val(),
        wc     : $('#wemake-color').val(),
        wa     : $('#wemake-alpha').val(),
        wf     : $('#wemake-font').val(),
        wfs    : $('#wemake-font-style').val(),
        ww     : $('#wemake-width').val(),
        p      : $('#porn').val(),
        px     : $('#porn-x').val(),
        py     : $('#porn-y').val(),
        ps     : $('#porn-size').val(),
        pc     : $('#porn-color').val(),
        pa     : $('#porn-alpha').val(),
        pf     : $('#porn-font').val(),
        pfs    : $('#porn-font-style').val(),
        pw     : $('#porn-width').val(),
        co     : $('#bgu-color').val(),
        gli    : $('#add-glitter').prop('checked'),
        bgt    : $('#bg-type').val(),
        rc     : $('#rect-color').val(),
        ra     : $('#rect-alpha').val(),
        bgr    : $('#bgr-color').val(),
        egr    : $('#egr-color').val(),
        or     : $('#gr-orientation').val(),
        bgi    : $('#bg-img').val(),
        cor    : $('#corners').val(),
        bth    : $('#border-thickness').val(),
        width  : $('#img-width').val(),
        height : $('#img-height').val(),
        x      : $('#custom-x').val(),
        y      : $('#custom-y').val(),
        s      : $('#custom-size').val(),
        bgu    : $('#custom-img-url').val(),
        r      : $('#add-rick').prop('checked'),
        rx     : $('#rick-x').val(),
        ry     : $('#rick-y').val(),
        rs     : $('#rick-size').val(),
        rr     : $('#rick-rad').val(),
        woc    : $('#wemake-outline-color').val(),
        wos    : $('#wemake-outline-size').val(),
        poc    : $('#porn-outline-color').val(),
        pos    : $('#porn-outline-size').val(),
    };
    localStorage.setItem('wemas', JSON.stringify(wemas));
}

function deleteItem(name) {
    var wemas = localStorage.getItem('wemas');
    if (wemas === null) {
        wemas = {};
    } else {
        wemas = JSON.parse(wemas);
        if (wemas[name] !== undefined && wemas[name] !== null) {
            delete wemas[name];
        }
    }
    localStorage.setItem('wemas', JSON.stringify(wemas));
}

function fillGallery() {
    var g     = $('#gallery');
    var wemas = localStorage.getItem('wemas');
    if (wemas === null) {
        wemas = {};
        localStorage.setItem('wemas', JSON.stringify(wemas));
    } else {
        wemas = JSON.parse(wemas);
    }

    g.empty();
    var keys = Object.keys(wemas).sort();
    if (keys.length === 0) {
        g.html('<h1>'+sorryNoGallery+'</h1>');
    } else {
        keys.forEach(function(k) {
            var v = wemas[k];
            var d = $('<div>')
            d.addClass('card');
            d.attr('data-item', k);

            var c = document.createElement('canvas');
            c.classList.add('img-fluid');
            c.setAttribute('width', v.width);
            c.setAttribute('height', v.height);
            var ctx = c.getContext('2d');
            drawCtx(true, ctx,
                v.rc, v.ra, v.ws, v.wc, v.wa, v.wf, v.wfs, v.w, v.wx, v.wy,
                v.ps, v.pc, v.pa, v.pf, v.pfs, v.p, v.px, v.py, v.gli,
                v.width, v.height, v.bth, v.bgt, v.co, v.cor, v.bgi, v.s,
                v.bgr, v.egr, v.org, v.x, v.y, v.bgu, v.r, v.rx, v.ry, v.rs,
                v.rr, v.ww, v.pw, v.woc, v.wos, v.poc, v.pos
            );
            d.html(c);

            var p = $('<div>');
            p.addClass('row text-center');
            var p1 = $('<div>');
            p1.addClass('col-sm-12 col-md-4');
            var a1 = $('<a>');
            a1.addClass('btn btn-info btn-block');
            var url = createUrl(new URL(window.location),
                v.w, v.wx, v.wy, v.ws, v.wc, v.wa, v.wf, v.wfs, v.ww,
                v.p, v.px, v.py, v.ps, v.pc, v.pa, v.pf, v.pfs, v.pw,
                v.co, v.ra, v.gli, v.bgt, v.rc, v.bgr, v.egr,
                v.or, v.bgi, v.cor, v.bth, v.width, v.height,
                v.x, v.y, v.s, v.bgu, v.r, v.rx, v.ry, v.rs, v.rr,
                v.woc, v.wos, v.poc, v.pos
            );
            a1.attr('href', url);
            a1.text(load);
            p1.append(a1);

            var p2 = $('<div>');
            p2.addClass('col-sm-12 col-md-4');
            var a2 = $('<a>');
            a2.addClass('btn btn-info btn-block');
            a2.attr('href', '#');
            a2.click(function(e) {
                this.download = $('#filename').val();
                if (v.bgu === undefined || v.bgu === null || v.bgu.match(/^ *$/)) {
                    this.href     = $('[data-item="'+k+'"] canvas')[0].toDataURL('image/octet-stream');
                } else {
                    e.preventDefault();
                    alert(downloadClic);
                }
            });
            a2.text(download);
            p2.append(a2);

            var p3 = $('<div>');
            p3.addClass('col-sm-12 col-md-4');
            var a3 = $('<a>');
            a3.addClass('btn btn-info btn-block');
            a3.attr('href', '#');
            a3.click(function(e) {
                e.preventDefault();
                deleteItem(k);
                $('[data-item="'+k+'"]').remove();
            });
            a3.text(deleteText);
            p3.append(a3);

            p.append(p1, p2, p3);
            d.append(p);

            g.append(d);
        });
    }
}

/* 
 * Helpers
 */
function show(id) {
    var b = $('#'+id);
    if (b.hasClass('d-none')) {
        b.removeClass('d-none');
    }
}

function hide(id) {
    var r = $('#'+id);
    if (!r.hasClass('d-none')) {
        r.addClass('d-none');
    }
}

function scalePreserveAspectRatio(imgW,imgH,maxW,maxH){
    return(Math.min((maxW/imgW),(maxH/imgH)));
}

function colorToRGBA(color, alpha) {
    var a = color.split('');
    r = parseInt(''+a[1]+a[2], 16);
    g = parseInt(''+a[3]+a[4], 16);
    b = parseInt(''+a[5]+a[6], 16);
    return 'rgba('+r+','+g+','+b+','+(1 - alpha/100)+')';
}

// Cookies
function createCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    }
    else {
        var expires = "";
    }

    document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca     = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1, c.length);
        }
        if (c.indexOf(nameEQ) == 0) {
            return c.substring(nameEQ.length, c.length);
        }
    }
    return null;
}

// center text
function centerText(e) {
    e.preventDefault();

    var wx  = $('#wemake-x');
    var wy  = $('#wemake-y');
    var px  = $('#porn-x');
    var py  = $('#porn-y');
    var width  = $('#img-width').val();
    var height = $('#img-height').val();

    wx.val(width / 2);
    wy.val(height / 4 + height * 3 / 20);
    px.val(width / 2);
    py.val(height * 3 / 4 + height / 8);

    refreshText();
}

// create the URL with all the parameters
function createUrl(url, w, wx, wy, ws, wc, wa, wf, wfs, ww, p, px, py, ps, pc, pa, pf, pfs, pw, co, ra, gli, bgt, rc, bgr, egr, or, bgi, cor, bth, width, height, x, y, s, bgu, r, rx, ry, rs, rr, woc, wos, poc, pos) {
    url.searchParams.set('w',   w);
    url.searchParams.set('wx',  wx);
    url.searchParams.set('wy',  wy);
    url.searchParams.set('ws',  ws);
    url.searchParams.set('wc',  wc);
    url.searchParams.set('wa',  wa);
    url.searchParams.set('wf',  wf);
    url.searchParams.set('wfs', wfs);
    url.searchParams.set('ww',  ww);
    url.searchParams.set('p',   p);
    url.searchParams.set('px',  px);
    url.searchParams.set('py',  py);
    url.searchParams.set('ps',  ps);
    url.searchParams.set('pc',  pc);
    url.searchParams.set('pa',  pa);
    url.searchParams.set('pf',  pf);
    url.searchParams.set('pfs', pfs);
    url.searchParams.set('pw',  pw);
    url.searchParams.set('co',  co);
    url.searchParams.set('ra',  ra);
    url.searchParams.set('gli', gli);
    url.searchParams.set('bgt', bgt);
    url.searchParams.set('rc',  rc);
    url.searchParams.set('bgr', bgr);
    url.searchParams.set('egr', egr);
    url.searchParams.set('or',  or);
    url.searchParams.set('bgi', bgi);
    url.searchParams.set('cor', cor);
    url.searchParams.set('bth', bth);
    url.searchParams.set('width', width);
    url.searchParams.set('height', height);
    url.searchParams.set('x', x);
    url.searchParams.set('y', y);
    url.searchParams.set('s', s);
    url.searchParams.set('bgu', bgu);
    url.searchParams.set('r',  r);
    url.searchParams.set('rx', rx);
    url.searchParams.set('ry', ry);
    url.searchParams.set('rs', rs);
    url.searchParams.set('rr', rr);
    url.searchParams.set('woc', woc);
    url.searchParams.set('wos', wos);
    url.searchParams.set('poc', poc);
    url.searchParams.set('pos', pos);

    return url;
}

function importData(f) {
    var reader = new FileReader();
    reader.addEventListener("loadend", function() {
        try {
            var newWemas = JSON.parse(String.fromCharCode.apply(null, new Uint8Array(reader.result)));
            var wemas    = localStorage.getItem('wemas');
            if (wemas === null) {
                localStorage.setItem('wemas', JSON.stringify(newWemas));
            } else {
                wemas = JSON.parse(wemas);
                $.extend(wemas, newWemas);
                localStorage.setItem('wemas', JSON.stringify(wemas));
            }
            fillGallery();

            alert(importOk);
        } catch(err) {
            alert(err);
        }
    });
    reader.readAsArrayBuffer(f[0]);
}

function exportData(e) {
    e.preventDefault();
    var a   = $('<a id="export-data-json">');
    a.hide();
    $('body').append(a);

    var storageData = [localStorage.getItem('wemas')];
    var exportFile  = new Blob(storageData, {type : 'application/json'});
    var url         = window.URL.createObjectURL(exportFile);

    a.attr('href', url);
    a.attr('download', 'export-data.json');
    $('#export-data-json')[0].click();
    $('#export-data-json').remove();
}
